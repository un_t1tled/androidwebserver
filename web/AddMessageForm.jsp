<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add message form</title>
    </head>
    <body>
        <div>
            <form action="/WebServerMNG/AddMessage" method="post">
                <table>
                    <tr>
                        <td>
                             <!--Message-->
                             <span class="msgBlock">Message:</span>
                        </td>
                        <td>
                            <input type="text" name="message" class="input"/>
                        </td>
                    </tr>
                    <tr>
                        <!--Send button-->
                        <td colspan="2" class="sendTd">
                            <input type="submit" name="send" value="Send" />
                        </td>
                    </tr>
                </table>
            </form>
        </div> 
    </body>
</html>
