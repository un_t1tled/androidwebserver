/**
 * check if all fields are not empty
 * @returns {Boolean} true if form is legal. false if not legal.
 */
function checkForm() {
    var usr = document.getElementById("username");
    var pass = document.getElementById("password");
    var full = document.getElementById("fullname");
    var email = document.getElementById("email");
    if (usr.value === "" || pass.value === "" || full.value === ""
            || email.value === "") {
        // Empty field were found
        alert("All fields must be filled up");
        return false;
    }
    return true;
}
/**
 * reset icon image to default
 * @returns {undefined}
 */
function resetImg() {
    var img = document.getElementById("iconImg");
    img.src = "/WebServerMNG/images/ic1.png";
}
/**
 * change user icon display image according to one of the 4 options
 * @returns {undefined}
 */
function changeIcon() {
    var index = document.getElementById("iconsList").selectedIndex;
    var img = document.getElementById("iconImg");
    var hiddenIndex = document.getElementById("hiddenIndex");

    switch (index) {
        case 0:
            img.src = "/WebServerMNG/images/ic1.png";
            hiddenIndex.value = 0;
            break;
        case 1:
            img.src = "/WebServerMNG/images/ic2.png";
            hiddenIndex.value = 1;
            break;
        case 2:
            img.src = "/WebServerMNG/images/ic3.png";
            hiddenIndex.value = 2;
            break;
        default:
            break;
    }

}
