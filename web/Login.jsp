<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Log in</title>
        <link rel="stylesheet" type="text/css" href="/WebServerMNG/css/login.css">
    </head>
    <body>
        <div>
            <form action="/WebServerMNG/Login" method="post" class="center">
                <table>
                    <tr>
                        <td>
                            <!--User name-->
                            <span class="msgBlock">User Name:</span>
                        </td>
                        <td>
                            <input type="text" name="username" placeholder="Example: admin" class="input" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <!--Password-->
                             <span class="msgBlock">Password:</span>
                        </td>
                        <td>
                            <input type="password" name="password" placeholder="Example: 1234" class="input"/>
                        </td>
                    </tr>
                    <tr>
                        <!--Send button-->
                        <td colspan="2" class="sendTd">
                            <input type="submit" name="send" class="send" value="Log in" /><span class="msgBlock"> OR <a href="/WebServerMNG/Register.jsp" >Register here!</a></span>
                        </td>
                    </tr>
                </table> 

            </form>
        </div> 
    </body>
</html>
