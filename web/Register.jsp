<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign up</title>
        <link rel="stylesheet" type="text/css" href="/WebServerMNG/css/register.css">
        <script src="/WebServerMNG/scripts/registerJS.js"></script>
    </head>
    <body>
        <div class="container">
            <!--Registration form-->
            <form action="/WebServerMNG/Register" method="post" >
                <table>
                    <tr>
                        <!--User name-->
                        <td>
                            User Name:
                        </td>
                        <td colspan="2"> 
                            <input type="text" id="username" name="username" placeholder="Example: usr1" class="input" />
                        </td>
                    </tr>
                    <tr>
                        <!--Password-->
                        <td>
                            Password:
                        </td>
                        <td colspan="2">
                            <input type="password" id="password" name="password" placeholder="Example: 1234" class="input"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!--Full name-->
                            Full Name:
                        </td>
                        <td colspan="2">
                            <input type="text" id="fullname" name="fullname" placeholder="Example: John Brais" class="input"/>
                        </td>
                    </tr>
                    <tr>
                        <!--Email-->
                        <td>
                            Email:
                        </td>
                        <td colspan="2">
                            <input type="text" id="email" name="email" placeholder="Example: John.Brais@gmail.com" class="input"/>
                        </td>
                    </tr>
                    <tr>
                        <!--User icon-->
                        <td>
                            Icon:
                        </td>
                        <td class="icon_td">
                            <select id="iconsList" name="iconsList" class="select" onchange="changeIcon()">
                                <option value="ic1">Icon 1</option>
                                <option value="ic2">Icon 2</option>
                                <option value="ic3">Icon 3</option>
                            </select>
                        <td>
                            <img id="iconImg" name="iconImg" src = "/WebServerMNG/images/ic1.png" height="40" width="40" />
                        </td>
                        </td>
                    </tr>
                    <tr>
                        <!--Send button-->
                        <td colspan="2" class="sendTd">
                            <input type="submit" name="send" class="send" onclick="return checkForm()" value="Register"/>
                        </td>
                        <!--Reset button-->
                        <td colspan="2" class="sendTd">
                            <input type="reset" name="reset" class="send" onclick="resetImg()" value="Reset"/>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="hiddenIndex" id="hiddenIndex" value="0"/>
            </form>
        </div> 
    </body>
</html>
