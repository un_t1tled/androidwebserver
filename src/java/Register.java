import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Register new user
 */
@WebServlet(name = "Register", urlPatterns = {"/Register"})
public class Register extends HttpServlet {
    /**
     * Add user to users database or show error is user 
     * already exist in database
     * @param request - Http Servlet Request
     * @param response - Http Servlet Response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String usr = request.getParameter("username");
        String pass = request.getParameter("password");
        String full = request.getParameter("fullname");
        String email = request.getParameter("email");
        int icon = Integer.parseInt(request.getParameter("hiddenIndex"));
        // Create a session
        SessionFactory sessionFactory; 
        sessionFactory = new Configuration().configure().buildSessionFactory();
        Session sess = sessionFactory.openSession();
        UserManager manager = new UserManager(sess);
        // Create new user
        User s = new User(usr, pass, full, email, icon);
        if (manager.ifUserExists(usr)) {
            // There is a user with this user name
            response.getOutputStream().print("FAILED");
        } else {
            // Add new user to database
            manager.addUser(s);
            HttpSession session = request.getSession();
            session.setAttribute("usr", s);
            response.getOutputStream().print("REGISTRED");
        }
        // Flush
        sess.flush();
    }
}
