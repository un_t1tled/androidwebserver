import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@WebServlet(urlPatterns = {"/GetLastMessages"})
public class GetLastMessages extends HttpServlet {
    /**
     * Returns 10 last messages from database.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SessionFactory sessionFactory;
        JSONObject jsonObj;
        JSONArray jsonArr;
        User usr;
        Message m;
        // Create session
        sessionFactory = new Configuration().configure().buildSessionFactory();
        Session sess = sessionFactory.openSession();
        MessageManager msgManager = new MessageManager(sess);
        // User manager
        UserManager userManager = new UserManager(sess);
        // Get 10 last messages
        List<Message> messagesList = msgManager.getLastMessages();
        Iterator<Message> it = messagesList.iterator();
        jsonArr = new JSONArray();
        // Iterate
        while(it.hasNext()) {
            m = it.next();
            jsonObj = new JSONObject();
            // Get user by his ID
            usr = userManager.getUserById(m.getUserId());
            jsonObj.put("message", m.getMessage());
            jsonObj.put("time", m.getTime());
            jsonObj.put("icon", usr.getIcon());
            jsonObj.put("username", usr.getUserName());
            jsonArr.add(jsonObj);
        }
        response.getOutputStream().print(jsonArr.toJSONString());
    }
}
