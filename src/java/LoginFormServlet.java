import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.json.simple.JSONObject;
/**
 * Login logic
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class LoginFormServlet extends HttpServlet {

    /**
     * Check if user login and has active session and 
     * redirect to the proper page
     * @param request - Http Servlet Request
     * @param response - Http Servlet Response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        // If user is not logged in
        if (session == null || session.getAttribute("usr") == null) {
            response.sendRedirect("Login.jsp");
        } else {
            response.sendRedirect("AddMessageForm.jsp");
        }
    }
    /**
     * Check if user in the users database. If user exists - send error, otherwise
     * returns user profile data in JSON format.
     * @param request - Http Servlet Request
     * @param response - Http Servlet Response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String usr = request.getParameter("username");
        String pass = request.getParameter("password");
        JSONObject jsonObject = new JSONObject();
        // Create a session
        SessionFactory sessionFactory; 
        sessionFactory = new Configuration().configure().buildSessionFactory();
        Session sess = sessionFactory.openSession();
        UserManager userManager = new UserManager(sess);
        if (userManager.ifUserExists(usr)) {
            User s = userManager.getUserByUserName(usr);
            if (s != null && s.getPassword().equals(pass)) {
                // Good login data
                HttpSession session = request.getSession();
                session.setAttribute("usr", s);
                jsonObject.put("username", s.getUserName());
                jsonObject.put("password", s.getPassword());
                jsonObject.put("icon", s.getIcon());
                jsonObject.put("fullname", s.getFullName());
                jsonObject.put("email", s.getEmail());
                // Put delivery status to 0 - LOGGED
                jsonObject.put("status", 0);
                response.getOutputStream().print(jsonObject.toJSONString());
                return;
            }
        }
        // Invalid login or username
        jsonObject.put("status", 1);
        response.getOutputStream().print(jsonObject.toJSONString());
    }
}
