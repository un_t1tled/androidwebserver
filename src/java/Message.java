/**
 * Message class
 */
public class Message {
    private int msgId;
    private String time;
    private int userId;
    private String message;
    
    public Message(String tm, int id, String msg) {
        this.time = tm;
        this.userId = id;
        this.message = msg;
    }
    public int getMsgId() {
        return this.msgId;
    }
    public String getTime() {
        return this.time;
    }
    public int getUserId() {
        return this.userId;
    }
    public String getMessage() {
        return this.message;
    }
    public void setMsgId(int id) {
        this.msgId = id;
    }
    public void setTime(String tm) {
        this.time = tm;
    }
    public void setUserId(int id) {
        this.userId = id;
    }
    public void setMessage(String msg) {
        this.message = msg;
    }
}
