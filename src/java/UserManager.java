import org.hibernate.Query;
import org.hibernate.Session;

public class UserManager {
    private Session session = null;
    
    public UserManager(Session session) {
        if(session == null)
            throw new RuntimeException("Invalid session object.");
        this.session = session;
    }
    /**
     * Add user to database
     * @param user the user to add
     */
    public void addUser(User user){
        session.save(user);
    }
    public void updateUser(User user){
        session.update(user);
    }
    /**
     * Delete user from database
     * @param user - user to delete
     */
    public void removeUser(User user) {
        session.delete(user);
    }
    /**
     * Check if user exist by user name
     * @param userName - user name
     * @return true if user in database, else false
     */
    public boolean ifUserExists(String userName) {
        String hql = "select userName from User where userName = :theName";
        Query q = session.createQuery(hql);
        q.setString("theName", userName);
        String c = (String) q.uniqueResult();
        return c != null;
    }
    public User getUserByUserName(String userName) {
        String hql = "from User where userName = :theName";
        Query q = session.createQuery(hql);
        q.setString("theName", userName);
        User c = (User) q.uniqueResult();
        return c;
    }
    public User getUserById(int id) {
        String hql = "from User where userId = :theName";
        Query q = session.createQuery(hql);
        q.setInteger("theName", id);
        User c = (User) q.uniqueResult();
        return c;
    }
}
