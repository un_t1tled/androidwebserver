/**
 * User class
 */
public class User {
    // User ID
    private int userId;
    // User name
    private String userName;
    // Password
    private String password;
    // Full name
    private String fullName;
    // Email
    private String email;
    // Path to user icon image
    private int icon;
    /**
     * Default constructor
     */
    public User(){
        userId = 0;
        // Default user
        userName = "admin";
        password = "1234";
        fullName = "Admin";
        email = "admin@walla.com";
        icon = 0;
    }
    /**
     * constructor that get all data as parameters
     * @param userName - username
     * @param password - password
     * @param fullName - fullname
     * @param email - email
     * @param icon - path to image
     */
    public User(String userName,String password,String fullName,
            String email, int icon){
        this.userName = userName;
        this.password = password;
        this.fullName = fullName;
        this.email = email;
        this.icon = icon;
    }
    /**
     * Get user name
     * @return user name
     */
    public String getUserName() {
        return this.userName;
    }
    /**
     * Get password
     * @return password
     */
    public String getPassword() {
        return this.password;
    }
    /**
     * Get full name
     * @return gull name
     */
    public String getFullName() {
        return this.fullName;
    }
    /**
     * Get email
     * @return email
     */
    public String getEmail() {
        return this.email;
    }
    /**
     * Get user ID
     * @return user ID
     */
    public int getUserId() {
        return this.userId;
    }
    /**
     * Get icon
     * @return icon
     */
    public int getIcon() {
        return this.icon;
    }
   /**
     * Set user name
     * @param usr user name
     */
    public void setUserName(String usr) {
        this.userName = usr;
    }
    /**
     * Set password
     * @param pass password
     */
    public void setPassword(String pass) {
        this.password = pass;
    }
    /**
     * Set full name
     * @param name full name
     */
    public void setFullName(String name) {
        this.fullName = name;
    }
    /**
     * Set email
     * @param email the user email
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * Set ID
     * @param id user id
     */
    public void setUserId(int id) {
        this.userId = id;
    }
    /**
     * Set icon path
     * @param icon icon
     */
    public void setIcon(int icon) {
        this.icon = icon;
    }

}
