import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.json.simple.JSONObject;
/**
 * AddMessage servlet
 */
@WebServlet(name = "AddMessage", urlPatterns = {"/AddMessage"})
public class AddMessage extends HttpServlet {
    /**
     * Adds message to the message table.
     * redirect to login page with an error message
     * @param request - Http Servlet Request
     * @param response - Http Servlet Response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        JSONObject jsonObject = new JSONObject();
        // Check if there is a valid session
        if (!(session == null || session.getAttribute("usr") == null)) {
            String msg = request.getParameter("message");
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            // Set time as string
            String time = sdf.format(c.getTime());
            // Create a session
            SessionFactory sessionFactory; 
            sessionFactory = new Configuration().configure().buildSessionFactory();
            Session sess = sessionFactory.openSession();
            // Creating a new message
            Message msgObj = new Message(time, ((User) session.getAttribute("usr")).getUserId(), msg);
            MessageManager msgManager = new MessageManager(sess);
            msgManager.addMessage(msgObj);
            // Flush
            sess.flush();
            // Set status to 0 - OK
            jsonObject.put("status", 0);
            // Set time
            jsonObject.put("time", time);
            response.getOutputStream().print(jsonObject.toJSONString());
            return;
        }
        // Failed
        jsonObject.put("status", 1);
        response.getOutputStream().print(jsonObject.toJSONString());
    }
}
