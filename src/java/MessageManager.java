import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class MessageManager {
    private Session session = null;
    
    public MessageManager(Session session) {
        if(session == null)
            throw new RuntimeException("Invalid session object.");
        this.session = session;
    }
    /**
     * Add user to database
     * @param msg the user to add
     */
    public void addMessage(Message msg){
        session.save(msg);
    }
    public void updateMessage(Message msg){
        session.update(msg);
    }
    /**
     * Delete user from database
     * @param msg - user to delete
     */
    public void removeMessage(Message msg) {
        session.delete(msg);
    }
    public List<Message> getLastMessages() {
        Message msg;
        Query query = session.createQuery("from MessageTable");
        List<Message> list = query.list();
        Iterator<Message> it=list.iterator();
        // Messages counter
        int i = 1;
        // Get 10 last messages
        while (it.hasNext() && i<=10) {
            msg = it.next();
            list.add(msg);
            i++;
        }
        return list;
    }
}
