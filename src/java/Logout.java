import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Logout current user
 */
@WebServlet(urlPatterns = {"/Logout"})
public class Logout extends HttpServlet {

    /**
     * Delete session and move user to login page
     * @param request - Http Servlet Request
     * @param response - Http Servlet Response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        request.getRequestDispatcher("Login.jsp").forward(request, response);
    }
}
